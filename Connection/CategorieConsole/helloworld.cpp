#include <string>
#include <vector>
#include <sstream> //istringstream
#include <iostream> // cout
#include <fstream> // ifstream

using namespace std;


vector<vector<string>> parse2DCsvFile(string inputFileName) {
 
    vector<vector<string> > data;
    ifstream inputFile(inputFileName);
    int compteur = 0;
    int compteurLigne = 0;
 
    while (inputFile) {
        compteur++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<string> record;
            int counter = 0;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ';'))
                    break;
                try {
                    counter++;
                    if(counter == 3){
                        compteurLigne ++;                       
                        if(compteurLigne == 1){
                            record.push_back(line);
                        }
                        double linetemp = stof(line);
                        if(linetemp >= 30){
                            record.push_back(line);
                        }
                        else
                        {
                            record.pop_back();
                            record.pop_back();
                        }
                        
                        counter = 0;
                    }
                    else
                    {
                        record.push_back(line);
                    }
                    
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << inputFileName << " line " << compteur
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << inputFileName << "\n";
    }
 
    return data;
}


int main()
{
    vector<vector<string>> data = parse2DCsvFile("Produit.csv");
 
    for (auto l : data) {
        for (auto x : l)
            cout << x << " ";
        cout << endl;
    }
    return 0;
}