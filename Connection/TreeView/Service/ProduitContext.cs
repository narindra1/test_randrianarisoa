﻿using ConnectionLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TreeView.Models;

namespace TreeView.Service
{
    public class ProduitContext
    {
        public List<Produits> GetProduits()
        {
            List<Produits> produits = new List<Produits>();
            using (SqlConnection connect = new SqlConnection(new ConnectionDAO().TestConnectionn()))
            {
                var query = "Select * From [dbo].[Produit]";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Produits produit = new Produits();
                                produit.Id = reader.GetInt32(0);
                                produit.Nom = reader.GetString(1);
                                produit.IdCategorie = reader.GetInt32(2);

                                produits.Add(produit);
                            }
                        }

                        return produits;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }
        public List<String> getCSVData()
        {
            List<Produits> resultat = this.GetProduits();
            List<String> liste = new List<string>();
            resultat.ForEach((item) =>
            {
                var arr = new List<String>() { item.Id.ToString(), item.Nom };
                liste.Add(String.Join(";", arr));
            });
            return liste;
        }


        public void Create(string nom, int idCategorie)
        {
            using (SqlConnection connect = new SqlConnection(new ConnectionDAO().TestConnectionn()))
            {
                var query = "insert into [dbo].[Produit] values (@nom, @idCategorie)";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("nom", nom));
                        command.Parameters.Add(new SqlParameter("idCategorie", idCategorie));
                        command.CommandText = query;
                        command.Connection = connect;
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }
    }
}