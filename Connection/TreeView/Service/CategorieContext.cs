﻿using ConnectionLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TreeView.Models;

namespace TreeView.Service
{
    public class CategorieContext
    {
        public List<Categorie> GetCategories()
        {
            List<Categorie> categories = new List<Categorie>();
            using (SqlConnection connect = new SqlConnection(new ConnectionDAO().TestConnectionn()))
            {
                var query = "Select * From [dbo].[Categorie]";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Categorie categorie = new Categorie();
                                categorie.Id = reader.GetInt32(0);
                                categorie.Libelle = reader.GetString(1);

                                categories.Add(categorie);
                            }
                        }

                        return categories;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }

        public void Create(string libelle)
        {
            using (SqlConnection connect = new SqlConnection(new ConnectionDAO().TestConnectionn()))
            {
                var query = "insert into [dbo].[Categorie] values (@libelle)";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("libelle", libelle));
                        command.CommandText = query;
                        command.Connection = connect;
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }

        public List<Element> GetNumberPercentage()
        {
            List<Element> elements = new List<Element>();
            using (SqlConnection connect = new SqlConnection(new ConnectionDAO().TestConnectionn()))
            {
                var query = "Select c.Libelle, count(p.IdCategorie) as Nombre from Produit p right join Categorie c on c.Id = p.IdCategorie group by p.IdCategorie, c.Libelle";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Element element = new Element();
                                element.libelle = reader.GetString(0);
                                element.nombre = reader.GetInt32(1);

                                elements.Add(element);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }

                    return elements;
                }
            }
        }

        public List<Element> GetPoucentage()
        {
            int total = new ProduitContext().GetProduits().Count;
            List<Element> elements = this.GetNumberPercentage();

            foreach(var item in elements)
            {
                double pourcentage = (item.nombre * 100) / total;
                item.pourcentage = pourcentage;
            }

            return elements;
        }

        public List<String> getCSVData()
        {
            List<Element> resultat = this.GetPoucentage();
            List<String> liste = new List<string>();
            resultat.ForEach((item) =>
            {
                var arr = new List<String>() { item.libelle, item.nombre.ToString(), item.pourcentage.ToString() };
                liste.Add(String.Join(";", arr));
            });
            return liste;
        }
    }
}