﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Services;
using TreeView.Models;
using TreeView.Service;

namespace TreeView.Controllers
{
    public class TreeViewController : ApiController
    {
        CategorieContext categorieContext = new CategorieContext();
        // GET: TreeView

        [Route("api/treeview/GetElements")]
        //[ScriptMethod(UseHttpGet =true, ResponseFormat = ResponseFormat.Json)]
        public IEnumerable<Element> GetElements()
        {
            return categorieContext.GetPoucentage();
        }
    }
}
