﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using TreeView.Models;
using TreeView.Service;

namespace TreeView.Controllers
{
    public class HomeController : Controller
    {
        ProduitContext produitContext = new ProduitContext();
        CategorieContext categorieContext = new CategorieContext();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var listcategorie = categorieContext.GetCategories();
            return View(listcategorie);
        }
        public JsonResult ListTree()
        {
            var listTree = new List<viewFormat>();
            var listproduit = produitContext.GetProduits();
            var listcategorie = categorieContext.GetCategories();

            foreach(var itemCateg in listcategorie)
            {
                listTree.Add(new viewFormat(itemCateg.Id.ToString(), "#", itemCateg.Libelle));
            }

            foreach(var itemProd in listproduit)
            {
                string id = "prod" + itemProd.Id;
                listTree.Add(new viewFormat(id, itemProd.IdCategorie.ToString(), itemProd.Nom));
            }
            var produit = Json(listTree, JsonRequestBehavior.AllowGet);
            return produit;

        }

        public ActionResult CreateProduit(string nom, string idCategorie)
        {
            if(nom != null && nom != "")
            {
                produitContext.Create(nom, Convert.ToInt32(idCategorie));
            }

            return RedirectToAction("Index");
        }

        public ActionResult CreateCategorie(string libelle)
        {
            if (libelle != null && libelle != "")
            {
                categorieContext.Create(libelle);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Export()
        {
            List<string> categorieList = categorieContext.getCSVData();

            StringBuilder sb = new StringBuilder();
            sb.Append("Libelle;Nombre;Poucentage (%)\r\n");
            categorieList.ForEach((valeur) =>
            {
                sb.Append(valeur + "\r\n");
            });
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "Produit.csv");
        }

    }
}
