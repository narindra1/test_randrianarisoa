﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeView.Models
{
    public class Categorie
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
    }
}