﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeView.Models
{
    public class Element
    {
        public string libelle { get; set; }
        public int nombre { get; set; }
        public double pourcentage { get; set; }
    }
}