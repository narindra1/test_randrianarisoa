﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeView.Models
{
    public class Produits
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int IdCategorie { get; set; }
    }
}