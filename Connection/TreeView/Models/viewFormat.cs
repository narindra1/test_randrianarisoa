﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeView.Models
{
    public class viewFormat
    {
       public string id { get; set; }
       public string parent { get; set; }
       public string text { get; set; }

        public viewFormat(string id, string parent, string text)
        {
            this.id = id;
            this.parent = parent;
            this.text = text;
        }
    }
}