Pour lancer l'application C#/ .NET: 

1) Lancer les requetes du fichier database.sql dans SQL Server
2) Changer la connectionString dans ...\Connection\ConnectionLibrary\ConnectionDAO.cs ligne 13 avec la connection de votre
base de données
3) Changer la connectionString dans ...\Connection\ConnectionLibrary\appsettings.json ligne 4 avec la connection de votre 
base de données
4) Changer le chemin absolu du fichier appsettings.json dans ...\Connection\ConnectionLibrary\ConnectionDAO.cs ligne 18 et 
21 avec le chemin du dossier téléchargé

Pour lancer l'application C++:
1) Copier le fichier Produit.csv dans le dossier du projet C++ au même niveau que le fichier helloworld.cpp
2) Lancer helloworld.exe dans un compilateur/exécuteur C++