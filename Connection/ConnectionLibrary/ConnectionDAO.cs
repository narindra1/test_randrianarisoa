﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace ConnectionLibrary
{
    public class ConnectionDAO
    {
        private static string ConnectionString = "Server=DESKTOP-NARINDR;Database=Shop;Trusted_Connection=True;";
        public SqlConnection TestConnection()
        {
            string connectionString = "";

            JObject o1 = JObject.Parse(File.ReadAllText(@"D:\Master\Development\Visual Studio 2019\Connection\ConnectionLibrary\appsettings.json"));

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(@"D:\Master\Development\Visual Studio 2019\Connection\ConnectionLibrary\appsettings.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                connectionString = o2.SelectToken("$.ConnectionStrings.ShopConnection").ToString();
            }
            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                try
                {
                    connect.Open();
                    Console.WriteLine("Connexion réussie !! ");
                    return connect;
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
        }

        public string TestConnectionn()
        {
            return ConnectionString;
        }
    }
}
