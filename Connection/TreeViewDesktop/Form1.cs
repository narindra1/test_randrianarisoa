﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TreeViewDesktop.Model;

namespace TreeViewDesktop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataUtil dataUtil = new DataUtil();
            List<Element> elements = dataUtil.callWebServices();

            foreach(var item in elements)
            {
                chart1.Series["Synthese"].Points.AddXY(item.libelle, item.pourcentage.ToString());
            }

            //chart1.Series["Synthese"].Points.AddXY("Ramesh", "8000");
            //chart1.Series["Synthese"].Points.AddXY("Ankit", "7000");
            //chart1.Series["Synthese"].Points.AddXY("Gurmeet", "10000");
            //chart1.Series["Synthese"].Points.AddXY("Suresh", "8500");
            ////chart title  
            chart1.Titles.Add("Synthese Chart");
        }
    }
}
