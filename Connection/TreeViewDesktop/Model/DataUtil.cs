﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewDesktop.Model
{
    public class DataUtil
    {        public string webGetMethod(string URL)
        {
            string jsonString = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;
            ((HttpWebRequest)request).UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)";
            request.Accept = "/";
            request.UseDefaultCredentials = true;
            request.Proxy.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/x-www-form-urlencoded";

            WebResponse response = request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            jsonString = sr.ReadToEnd();
            sr.Close();
            return jsonString;
        }

        public List<Element> callWebServices()
        {
            string URL = "https://localhost:44370/api/treeview/GetElements";
            var data = webGetMethod(URL);

            JArray o = JArray.Parse(data);
            List<Element> elements = ((JArray)o).Select(e => new Element
            {
                libelle = e["libelle"].ToString(),
                pourcentage = Convert.ToDouble(e["pourcentage"])
            }).ToList();
            Console.WriteLine(o);

            return elements;
        }

    }
}
